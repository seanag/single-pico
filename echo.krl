ruleset echo {
  meta {
    name "echo"
    description <<
      Playing with an echo endpoint
    >>
    author "Sean George"
    logging on
    shares __testing
  }

  global {
    __testing = { "events": [ { "domain": "echo", "type": "hello" },
                            { "domain": "echo", "type": "message",
                              "attrs": [ "input" ] } ] }
  }

  rule hello is active {
    select when echo hello
    send_directive("say") with
      something = "Hello World"
    always {
      "This is my log message".klog("")
    }
  }

  rule message is active {
    select when echo message input re#(.*)# setting(m);
    send_directive("say") with
      something = m
  }
}