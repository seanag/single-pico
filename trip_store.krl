ruleset trip_store {
  meta {
    name "trip_store"
    description <<
      Playing with trip_store
    >>
    author "Sean George"
    logging on
    provides trips, long_trips, short_trips
    shares trips, long_trips, short_trips
  }

  global {
    clear_trip = {"-1":"-1"}

    trips = function() {
      ent:trips
    }

    long_trips = function() {
      ent:long_trips
    }

    short_trips = function () {
      tripMileages = ent:trips.klog("tripMileages is : ");
      longTripMileages = ent:long_trips.klog("longTripmileages is: ");
      trips = tripMileages.values().filter(function(x){x.as("Number") < 50 && x.as("Number") != -1})
    }
  }

  rule collect_trips is active {
    select when explicit trip_processed
    pre {
      mileage = event:attr("mileage").as("Number").klog("mileage is: ")
      timestamp = time:now()
    }
    always {
      ent:trips{[timestamp]} := mileage
    }
   
  }

  rule collect_long_trips is active {
    select when explicit found_long_trip
    pre {
      mileage = event:attr("mileage").as("Number").klog("mileage is: ")
      timestamp = time:now()
    }
    always {
      ent:long_trips{[timestamp]} := mileage
    }
  }

  rule clear_names is active {
    select when car trip_reset
    always {
      ent:trips := clear_trip.klog("resetting trips");
      ent:long_trips := clear_trip

    }
  }
}