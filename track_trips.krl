ruleset track_trips {
  meta {
    name "track_trips"
    description <<
      Playing with track_trips
    >>
    author "Sean George"
    logging on
    shares __testing
  }

  rule process_trip is active {
    select when echo message mileage re#(.*)# setting(mileage);
    send_directive("trip") with
      trip_length = mileage
  }
}