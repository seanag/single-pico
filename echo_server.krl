ruleset echo_server {
  meta {
    name "echo"
    description <<
      Playing with an echo endpoint
    >>
    author "Phil Windley"
    logging on
    shares __testing
  }

  global {
    __testing = { "events": [ { "domain": "echo", "type": "hello" },
                            { "domain": "echo", "type": "message",
                              "attrs": [ "input" ] } ] }
  }

  rule hello_world is active {
    select when echo hello
    send_directive("say") with
      something = "Hello CS462 Again"
    always {
      "This is my log message".klog("")
    }
  }
  
  rule echo is active {
    select when echo message input re#(.*)# setting(m);
    send_directive("say") with
      something = m
  }
}